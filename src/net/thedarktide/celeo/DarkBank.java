package net.thedarktide.celeo;

import java.util.logging.Logger;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * <b>DarkBank</b><br>
 * Started on July 20th, 2012.
 * @author Celeo
 */
public class DarkBank extends JavaPlugin
{

	private final Logger log = Logger.getLogger("Minecraft");
	private final boolean DEBUGGING = true;
	private Bank bank = null;

	@Override
	public void onEnable()
	{
		getDataFolder().mkdirs();
		bank = new Bank(this);
		log("Enabled");
	}

	@Override
	public void onDisable()
	{
		bank.close();
		log("Disabled");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!hasPermission(sender, "darkbank.control"))
		{
			((Player) sender).sendMessage("�cYou cannot use that command.");
			return true;
		}
		if (args == null || args.length < 2)
		{
			tellSender(sender, "�e/darkbank �9check|add|subtract|set �a[player] [value]");
			return true;
		}
		String param = args[0].toLowerCase();
		if (param.equals("check"))
		{
			String player = args[1];
			tellSender(sender, "�eValue for �9" + player + " �eis�a " + bank.getBalance(player));
			return true;
		}
		else if (param.equals("add") && args.length == 3)
		{
			String player = args[1];
			String value = args[2];
			if (isDouble(value))
			{
				bank.changeBalance(player, value);
				return true;
			}
			tellSender(sender, "�cYou must set a number as the value in the command.");
			return true;
		}
		else if (param.equals("subtract") && args.length == 3)
		{
			String player = args[1];
			String value = args[2].replace("-", "");
			if (isDouble(value))
			{
				bank.changeBalance(player, "-" + value);
				return true;
			}
			tellSender(sender, "�cYou must set a number as the value in the command.");
			return true;
		}
		else if (param.equals("set") && args.length == 3)
		{
			String player = args[1];
			String value = args[2];
			if (isDouble(value.replace("-", "")))
			{
				bank.setBalance(player, value);
				return true;
			}
			tellSender(sender, "�cYou must set a number as the value in the command.");
			return true;
		}
		tellSender(sender, "�e/darkbank �9check|add|subtract|set �a[player] [value]");
		return true;
	}

	@SuppressWarnings("static-method")
	public boolean hasPermission(CommandSender sender, String node)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;
			if (player.isOp())
				return true;
			if (player.hasPermission(node))
				return true;
			return false;
		}
		return true;
	}

	public void tellSender(CommandSender sender, String message)
	{
		if (sender instanceof Player)
		{
			((Player) sender).sendMessage(message);
		}
		else
		{
			log(message.replaceAll("�[0-9a-f]", ""));
		}
	}

	public static boolean isInt(String string)
	{
		try
		{
			Integer.parseInt(string);
			return true;
		}
		catch (NumberFormatException e) { }
		return false;
	}

	public static boolean isDouble(String string)
	{
		try
		{
			Double.parseDouble(string);
			return true;
		}
		catch (NumberFormatException e) { }
		return false;
	}

	public Double getBalanceFor(Player player)
	{
		return getBalanceFor(player.getName());
	}

	public Double getBalanceFor(String player)
	{
		return bank.getBalance(player);
	}

	public void log(String message)
	{
		log.info("[DarkBank] " + message);
	}

	public boolean debug(String message)
	{
		if (DEBUGGING)
			log("<DEBUG> " + message);
		return DEBUGGING;
	}

}