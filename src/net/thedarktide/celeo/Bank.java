package net.thedarktide.celeo;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * <b>Bank</b> object for controlling player information
 * @author Celeo
 */
public class Bank
{

	final DarkBank plugin;
	Connection connection = null;
	Map<String, Double> balances = new HashMap<String, Double>();

	public Bank(DarkBank instance)
	{
		plugin = instance;
		loadFromFile();
	}

	private void loadFromFile()
	{
		try
		{
			File file = new File(plugin.getDataFolder(), "/database.db");
			if (!file.exists())
				file.createNewFile();
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + file);
			Statement stat = connection.createStatement();
			stat.executeUpdate("Create table if not exists `holdings` (player VARCHAR(50), balance DOUBLE)");
			ResultSet rs = stat.executeQuery("Select * from `holdings`");
			while (rs.next())
				balances.put(rs.getString("player"), Double.valueOf(rs.getDouble("balance")));
			stat.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public void close()
	{
		try
		{
			connection.close();
		}
		catch (SQLException e) { }
	}

	public void saveBalance(String player)
	{
		try
		{
			Statement stat = connection.createStatement();
			stat.executeUpdate("Update `holdings` set `balance`=" + getBalance(player) + "where `player`='" + player + "'");
			stat.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public Double changeBalance(String player, String value)
	{
		balances.put(player, Double.valueOf(getBalance(player).toString() + value));
		saveBalance(player);
		return getBalance(player);
	}

	public Double setBalance(String player, String value)
	{
		balances.put(player, Double.valueOf(value));
		saveBalance(player);
		return getBalance(player);
	}

	public boolean hasAccount(String player)
	{
		return balances.containsKey(player);
	}

	public Double getBalance(String player)
	{
		return hasAccount(player) ? balances.get(player) : Double.valueOf(0.0);
	}

}